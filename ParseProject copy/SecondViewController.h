//
//  SecondViewController.h
//  ParseProject
//
//  Created by Randy Jorgensen on 3/4/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface SecondViewController : UIViewController <UITextViewDelegate>
{
    NSString *responseString;
    UIView* sv;
    NSString *color;
    NSString *name;
    UILabel *label6;
    UITextView *label7;
    UILabel *label1;
    UITextView *label8;
}
@property (nonatomic, retain) UITextView *label7;
@property (nonatomic, retain) UITextView *label8;

@end
