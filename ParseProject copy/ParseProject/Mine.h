//
//  Mine.h
//  ParseProject
//
//  Created by Randy Jorgensen on 3/4/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Mine: NSObject
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* color;


@end