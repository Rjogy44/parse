//
//  ViewController.m
//  ParseProject
//
//  Created by Randy Jorgensen on 3/4/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (IBAction)btn1:(id)sender {
    
    SecondViewController *svc = [SecondViewController new];
    
    [self.navigationController pushViewController:svc animated:YES];

}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self post];
    
    
    btn1 = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn1 setTitle:@"Add New Name & Color" forState:UIControlStateNormal];
    
    cView=[[UITableView alloc] initWithFrame:self.view.frame];
    [cView setDataSource:self];
    [cView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:cView];
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}




-(void)post {
    NSURLSession* session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:nil];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://adamsparsetest.herokuapp.com/parse/classes/Game"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    
    
    NSDictionary* d = @{@"Game":@"playerName"};
    
//
    NSError* error;

    
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"adamsappid" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setHTTPMethod:@"GET"];
    
    
    NSURLSessionDataTask* postDataTask = [session dataTaskWithRequest: request completionHandler:^(NSData* data, NSURLResponse* response, NSError* error){
        results1 = [NSMutableArray new];
        dict1 = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        NSArray *results = dict1[@"results"];
        results1 = [results valueForKey:@"name"];
        results2 = [results valueForKey:@"color"];
    
        
        
        
        [cView reloadData];
        
    
    
    }]; [postDataTask resume];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return results1.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell = [cell initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"cell"];
    id object = results1[indexPath.row];// similar to [myArray objectAtIndex:0]
    NSString *str = [[NSString alloc] init];
    if([object isEqual:[NSNull null]])
    {
        str = @"Empty Name";
    }
    else
    {
        str = results1[indexPath.row];
    }
    id object1 = results2[indexPath.row];// similar to [myArray objectAtIndex:0]
    NSString *str1 = [[NSString alloc] init];
    if([object1 isEqual:[NSNull null]])
    {
        str1 = @"Empty Name";
    }
    else
    {
        str1 = results2[indexPath.row];
    }
    
    
    cell.textLabel.text = str;
    cell.detailTextLabel.text = str1;
    
    return cell;
    
    

}







- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

