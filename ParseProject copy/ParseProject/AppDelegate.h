//
//  AppDelegate.h
//  ParseProject
//
//  Created by Randy Jorgensen on 3/4/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

