//
//  ViewController.h
//  ParseProject
//
//  Created by Randy Jorgensen on 3/4/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Mine.h"
#import "SecondViewController.h"
@interface ViewController : UIViewController <UITableViewDataSource>
{
    
    __weak IBOutlet UIButton *btn1;
    NSDictionary* dict1;
    NSMutableArray *results1;
    NSMutableArray *results2;
    NSString *name;
    NSString *color;
    NSArray *array;
    UITableView* cView;

    UITableViewCell *cell;
}


@end

