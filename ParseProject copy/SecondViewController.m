//
//  SecondViewController.m
//  ParseProject
//
//  Created by Randy Jorgensen on 3/4/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    sv = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor greenColor];
    [self.view addSubview:sv];
    
    label6 = [[UILabel alloc]initWithFrame:CGRectMake(10, 140, 300, 40)];
    label6.text = @"Name";
    [label6 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    [sv addSubview:label6];
    
    label7 = [[UITextView alloc]initWithFrame:CGRectMake(10, 200, 300, 40)];
    [label7 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
//    label7.text = @"Enter Name Here";
    label7.delegate = self;
    label7.backgroundColor = [UIColor whiteColor];
    [sv addSubview:label7];
    
    label1 = [[UILabel alloc]initWithFrame:CGRectMake(10, 260, 300, 40)];
    label1.text = @"Color";
    [label1 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
    [sv addSubview:label1];

    
    label8 = [[UITextView alloc]initWithFrame:CGRectMake(10, 320, 300, 40)];
    [label8 setFont: [UIFont fontWithName:@"ChalkboardSE-Bold" size:20]];
//    label8.text = @"Enter Color Here";
    label8.delegate = self;
    label8.backgroundColor = [UIColor whiteColor];
    [sv addSubview:label8];
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn1 setTitle:@"Save" forState:UIControlStateNormal];
    btn1.frame = CGRectMake(10, 380, 300, 40);
    btn1.backgroundColor = [UIColor whiteColor];
    [btn1 addTarget:self action:@selector(btntouched:) forControlEvents:UIControlEventTouchUpInside];
    [sv addSubview:btn1];
    

}
-(void)btntouched:(UIButton*)btn {
    
   
    [self post];
    ViewController *vc = [ViewController new];
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    name = label7.text;
    
    color = label8.text;
}



-(void)post {
    NSURLSession* session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration] delegate:nil delegateQueue:nil];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://adamsparsetest.herokuapp.com/parse/classes/Game"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60];
    
    
    NSDictionary* d = @{@"name":name,@"color":color};
    NSError* error;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:d options:NSJSONWritingPrettyPrinted error:&error];
    
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"adamsappid" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setHTTPBody:jsonData];
    [request setHTTPMethod:@"POST"];
    
    
    NSURLSessionDataTask* postDataTask = [session dataTaskWithRequest: request completionHandler:^(NSData* data, NSURLResponse* response, NSError* error){
        
        responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        
    }]; [postDataTask resume];
}

@end
